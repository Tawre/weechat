const mainContainer = document.querySelector('main[data-room-id]');
const participansContainer = document.querySelector('#participans-container');
const messagesContainer = document.querySelector('#messages-container');

function refreshRoom() {

  if (mainContainer) {
    const roomId = mainContainer.getAttribute('data-room-id');
    const url = '/api/room/' + roomId + '/refresh';
    fetch(url).catch(
        function reject () {
          console.log('There is an communication error');
        }
    )
        .then(response => response.json())
        .then(function(json) {
          
      regenerateUsers(json.participans);
      regenerateMessages(json.messages);

      return true;
    }).then(function repeat() {
      setTimeout(refreshRoom, 5000);
    })
  }


}

async function regenerateUsers(users) {

  participansContainer.innerHTML = "";

  for (let i = 0; i < users.length; i++) {
    let participanElement = createUserBox(users[i]);
    participansContainer.appendChild(participanElement);
  }
}

function createUserBox(user) {
  const boxContainer = document.createElement('div');
  boxContainer.classList.add('list-group-item','list-group-item-action');
  boxContainer.innerText = user.username + ' ';

  const statusIndicator = document.createElement('span');
  statusIndicator.classList.add('badge', 'badge-pill', user['is-online'] ? 'badge-success' : 'badge-danger');
  statusIndicator.innerHTML = '&nbsp;';
  boxContainer.appendChild(statusIndicator);

  return boxContainer;
}

async function regenerateMessages(messages) {
  messagesContainer.innerHTML = "";

  for (let i = 0; i < messages.length; i++) {
    let messageElement = createMessageBox(messages[i]);
    messagesContainer.appendChild(messageElement);
  }
}

function createMessageBox(message) {
  const boxContainer = document.createElement('div');
  boxContainer.classList.add('list-group-item','list-group-item-action', 'row');

  const usernameContainer = document.createElement('div');
  usernameContainer.classList.add('col-4');
  usernameContainer.innerText = message.sender + ' ';

  if (message['can-delete']) {
    const deleteLink = document.createElement('a');
    deleteLink.innerHTML = '&lbrack;Delete&rbrack;';
    deleteLink.setAttribute('href', '/message/delete/' + message.id);
    usernameContainer.appendChild(deleteLink)
  }
  boxContainer.appendChild(usernameContainer);

  const messageContainer = document.createElement('div');
  messageContainer.classList.add('col-auto');
  messageContainer.innerText = message.message;
  boxContainer.appendChild(messageContainer);

  return boxContainer;
}


setTimeout(refreshRoom, 5000);



