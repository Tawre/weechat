const fieldUsername = document.querySelector('#username');
const fieldEmail = document.querySelector('#email');
const fieldPassword = document.querySelector('#password');
const fieldPasswordAgain = document.querySelector('#password_again');
const alerts = document.querySelector('#alerts');

fieldUsername.onkeyup = checkRequiredField;
fieldEmail.onkeyup = checkRequiredField;

fieldPassword.onkeyup = checkPasswords;
fieldPasswordAgain.onkeyup = checkPasswords;

function checkPasswords(event) {
  deleteMessages(fieldPassword);
  deleteMessages(fieldPasswordAgain);

  if (fieldPassword.value != fieldPasswordAgain.value) {
    let alert = document.createElement('p');
    alert.innerText = 'Passwords are not same';
    alert.setAttribute('data-for-field', fieldPassword.id);
    alerts.appendChild(alert);
  }

  if (fieldPassword.value.length < 6) {
    let alert = document.createElement('p');
    alert.innerText = 'Password is too short, needs 6 characters as minimum';
    alert.setAttribute('data-for-field', fieldPassword.id);
    alerts.appendChild(alert);
  }

  if (alerts.childElementCount > 0) {
    alerts.classList.remove('invisible');
    fieldPassword.classList.add('is-invalid');
    fieldPasswordAgain.classList.add('is-invalid');
  }

  checkRequiredField(event);
}

function checkRequiredField(event) {
  deleteMessages(event.target);

  if (event.target.value == '') {
    let fieldName = event.target.parentNode.querySelector('label').innerText;
    let alert = document.createElement('p');


    alert.innerText = fieldName + ' is required';
    alert.setAttribute('data-for-field', event.target.id);
    alerts.appendChild(alert);
    alerts.classList.remove('invisible');

    event.target.classList.add('is-invalid');
  }

  if (alerts.childElementCount > 0) {
    alerts.classList.remove('invisible');
  } else {
    alerts.classList.add('invisible');
  }
}


function deleteMessages(element) {
  let element_alerts = document.querySelectorAll('.alert p[data-for-field="' + element.id + '"]');
  if (element_alerts !== null) {

    for (let i = element_alerts.length - 1; i > -1 ; i--) {
      element_alerts.item(i).remove();
    }
  }
  if (element.classList.contains('is-invalid')) {
    element.classList.remove('is-invalid')
  }
}
