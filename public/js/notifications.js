const notifyPanel = document.querySelector('#notify-panel');
const notifyDashboard = document.querySelector('#notify-dashboard');

function processNotifications() {
  const url = '/api/notifications';

  fetch(url).catch(
      function reject() {
        console.log('There is an communication error');
      }
  )
      .then(response => response.json())
      .then(function processPanel(json) {

        if (notifyPanel) {
          const badge = notifyPanel.querySelector('.badge');
          badge.innerText = json.unreaded;
          if (json.unreaded > 0) {
            notifyPanel.classList.add('active');
            badge.classList.remove('invisible')
          }
          else {
            notifyPanel.classList.remove('active');
            badge.classList.add('invisible')
          }
        }


        return json;
      })
      .then(function processDashboard(json) {

        if (notifyDashboard) {
          const readAllLink = notifyDashboard.querySelector('.card-link');
          if (json.unreaded > 0) {
            readAllLink.classList.remove('invisible');
          }
          else {
            readAllLink.classList.add('invisible');
          }

          const list = notifyDashboard.querySelector('.list-group');
          list.innerHTML = "";
          for (let i = 0; i < json.notifications.length; i++) {
            let notificationElement = createNotificationBox(json.notifications[i]);
            list.appendChild(notificationElement);
          }
        }

        return json;
      })
      .then(function repeat() {
        setTimeout(processNotifications, 5000);
      });
}

setTimeout(processNotifications, 5000);

function createNotificationBox(notification) {
  const boxContainer = document.createElement('a');
  boxContainer.classList.add('list-group-item', 'list-group-item-action');
  boxContainer.innerText = notification.message;
  boxContainer.setAttribute('href', notification.link);

  if(!notification.readed) {
    boxContainer.classList.add('active');
  }

  return boxContainer;
}
