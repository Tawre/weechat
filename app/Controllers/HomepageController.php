<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Controllers;


use WeeChat\Core\Ui\Controllers\BaseController;

class HomepageController extends BaseController {

  protected function preExecute() {
    if ($this->currentUser->isAuthenticated()) {
      $this->redirectTo('/rooms');
    }
  }


}
