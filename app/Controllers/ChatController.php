<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Controllers;


use WeeChat\Core\Chat\Notification\Event\EventNewMessage;
use WeeChat\Core\Http\Exception\NotAcceptable;
use WeeChat\Core\Http\JsonResponse;
use WeeChat\Core\Services\AssetsService;
use WeeChat\Core\Ui\Controllers\BaseController;

class ChatController extends BaseController {

  /** @var \WeeChat\Core\Repositories\UserRepository */
  private $userRepository;

  /** @var \WeeChat\Core\Repositories\NotificationRepository */
  private $notificationRepository;

  /** @var \WeeChat\Core\Repositories\RoomRepository */
  private $roomsRepository;

  /** @var \WeeChat\Core\Repositories\MessageRepository */
  private $messageRepository;

  protected function preExecute() {

    if (!$this->currentUser->isAuthenticated()) {
      $this->redirectTo('/sign-in');
    }

    $this->roomsRepository = $this->container->getService('rooms');
    $this->userRepository = $this->container->getService('users');
    $this->notificationRepository = $this->container->getService('notifications');
    $this->messageRepository = $this->container->getService('messages');
  }


  public function renderDefault() {
    $this->template->rooms = $this->roomsRepository->getAllPublic();
    $this->template->users = $this->userRepository->getAllOther($this->currentUser);
    $this->template->notifications = $this->notificationRepository->findForUser($this->currentUser);
  }

  public function preRenderRoom($id) {
    $message = filter_input(INPUT_POST, 'message');
    /** @var \WeeChat\Core\Chat\ChatRoom $room */
    $room = $this->roomsRepository->getRoom($id);

    if (empty($room)) {
      $this->redirectTo('/not-found');
    }

    if ($message && $message = trim($message)) {
      $this->messageRepository->addMessage($message, $this->currentUser, $room);
      $event = new EventNewMessage($this->currentUser, $room);
      $this->notificationRepository->addToRoom($room, $event, $this->currentUser);

      $this->redirectTo("/room/{$room->getId()}");
    }
  }

  public function renderRoom($id) {

    /** @var \WeeChat\Core\Chat\ChatRoom $room */
    $room = $this->roomsRepository->getRoom($id);

    $this->roomsRepository->addParticipant($room, $this->currentUser);
    $this->notificationRepository->readRoomNotifications($this->currentUser,$room);

    $this->template->roomId = $room->getId();
    $this->template->roomName = $room->getName();
    $this->template->participans = $room->getParticipants();
    $this->template->messages = $room->getMessages();

    /** @var AssetsService $assetsManager */
    $assetsManager = $this->container->getService('template_assets');

    $assetsManager->addJS('js/room.js');
  }

  public function renderRefreshRoom($id) {
    /** @var \WeeChat\Core\Chat\ChatRoom $room */
    $room = $this->roomsRepository->getRoom($id);

    $roomName = $room->getName();
    $participans = $room->getParticipants();
    $messages = $room->getMessages();

    /** @var \WeeChat\Core\Chat\Message\TextMessage $message */
    foreach ($messages as &$message) {
      $message = $message->toArray();
    }

    /** @var \WeeChat\Core\User\UserInterface $participan */
    foreach ($participans as &$participan) {
      $participan = $participan->toArray();
    }

    return new JsonResponse([
      'room-name' => $roomName,
      'participans' => $participans,
      'messages' => $messages,
    ]);
  }

  public function preRenderCreatePublicRoom() {
    $roomName = filter_input(INPUT_POST, 'room_name');

    if (!empty($roomName)) {
      $room = $this->roomsRepository->createRoom($roomName, $this->currentUser);
      $this->redirectTo("/room/{$room->getId()}");
    }
  }

  public function preRenderCreatePrivateRoom($user_id) {
    $receiver = $this->userRepository->get($user_id);



    if (empty($receiver)) {
      throw new NotAcceptable();
    }
    $room = $this->roomsRepository->getPrivateRoom($this->currentUser, $receiver);

    $this->redirectTo("/room/{$room->getId()}");
  }

}
