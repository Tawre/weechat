<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Controllers;


use PDO;
use WeeChat\Core\Services\AssetsService;
use WeeChat\Core\Ui\Controllers\BaseController;

class SignController extends BaseController {

  /** @var \WeeChat\Core\Repositories\UserRepository */
  private $userRepository;

  protected function preExecute() {
    $this->userRepository = $this->container->getService('users');
  }


  public function preRenderIn() {
    $formSubmitted = filter_input(INPUT_POST, 'login_form_submitted', FILTER_VALIDATE_BOOLEAN);

    $this->template->errors = $formSubmitted ? $this->validateLoginForm() : [];

    $formValidated = $formSubmitted && empty($this->template->errors);

    if ($this->currentUser->isAuthenticated() || $formValidated) {
      $this->submitLoginForm();
    }
  }

  public function renderIn() {
    $this->template->title = "Sign In";
  }

  public function preRenderOn() {
    $formSubmitted = filter_input(INPUT_POST, 'register_form_submitted', FILTER_VALIDATE_BOOLEAN);

    $this->template->errors = $formSubmitted ? $this->validateRegisterForm() : [];

    if ($formSubmitted && empty($this->template->errors)) {
      $this->submitRegisterForm();
    }

    /** @var AssetsService $assetsManager */
    $assetsManager = $this->container->getService('template_assets');

    $assetsManager->addJS('js/register.js');
  }

  public function renderOn() {
    $this->template->title = "Sign On";
  }

  public function preRenderOut() {
    /** @var \WeeChat\Core\Repositories\UserRepository $userRepositary */
    $userRepositary = $this->container->getService('users');

    $userRepositary->logout();

    $this->redirectTo('/');
  }

  private function validateRegisterForm() {
    $errors = [];

    #region Username validation
    $username = filter_input(INPUT_POST, 'username');

    if ($username === NULL) {
      $errors['username'][] = 'Username is required field';
    }
    elseif ($username === FALSE) {
      $errors['username'][] = 'Username is invalid';
    }
    #endregion

    #region E-mail validation
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

    if ($email === NULL) {
      $errors['email'][] = 'E-mail is required field';
    }
    elseif ($email === FALSE) {
      $errors['email'][] = 'E-mail is invalid';
    }
    #endregion

    #region Password validation
    $password = filter_input(INPUT_POST, 'password');
    $password_again = filter_input(INPUT_POST, 'password_again');

    if ($password === NULL) {
      $errors['password'][] = 'Password is required field';
    }
    else {
      if ($password !== $password_again) {
        $errors['password'][] = 'Passwords are not same';
      }

      if (strlen($password) < 6) {
        $errors['password'][] = 'Password is too short, needs 6 characters as minimum';
      }
    }

    if ($password_again === NULL) {
      $errors['password_again'][] = 'Password Repeat is required field';
    }
    #endregion

    return $errors;
  }

  private function submitRegisterForm() {
    $username = filter_input(INPUT_POST, 'username');
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $password = filter_input(INPUT_POST, 'password');

    $this->userRepository->register($username, $password, $email);
    $this->redirectTo('/sign-in');
  }

  private function validateLoginForm($user) {
    $errors = [];

    #region Username validation
    $username = filter_input(INPUT_POST, 'username');

    if ($username === NULL) {
      $errors[] = 'Username is required field';
    }

    #endregion

    #region Password validation
    $password = filter_input(INPUT_POST, 'password');

    /** @var \WeeChat\Core\Repositories\UserRepository $userRepositary */
    $userRepositary = $this->container->getService('users');

    $user = $userRepositary->login($username, $password);

    if ($password === NULL) {
      $errors[] = 'Password is required field';
    }
    elseif (!$user) {
      $errors[] = 'There is something wrong, check your username and password';
    }
    #endregion
  }

  private function submitLoginForm() {
    $this->redirectTo('/rooms');
  }

}
