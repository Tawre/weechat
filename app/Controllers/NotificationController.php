<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Controllers;


use WeeChat\Core\Http\JsonResponse;
use WeeChat\Core\Ui\Controllers\BaseController;

class NotificationController extends BaseController {


  /** @var \WeeChat\Core\Repositories\RoomRepository */
  private $roomsRepository;

  /** @var \WeeChat\Core\Repositories\UserRepository */
  private $userRepository;

  /** @var \WeeChat\Core\Repositories\NotificationRepository */
  private $notificationRepository;



  protected function preExecute() {

    if (!$this->currentUser->isAuthenticated()) {
      (new JsonResponse(['error' => 'Not authorized'], [], 403))->send();
      exit();
    }

    $this->roomsRepository = $this->container->getService('rooms');
    $this->userRepository = $this->container->getService('users');
    $this->notificationRepository = $this->container->getService('notifications');
  }

  public function renderUserNotifications() {

    $notifications = $this->notificationRepository->findForUser($this->currentUser);

    if ($notifications) {
      /** @var \WeeChat\Core\Chat\Notification\NotificationInterface $notification */
      foreach ($notifications as &$notification) {
        $notification = $notification->toArray();
      }
    }

    return [
      'unreaded' => $this->notificationRepository->countUnreadedForUser($this->currentUser),
      'notifications' => $notifications
    ];
  }

  public function renderUserMarkAllReaded() {
    $this->notificationRepository->readAllNotifications($this->currentUser);
    $this->redirectToReferrer();
  }
}
