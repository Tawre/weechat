<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Controllers;


use WeeChat\Core\Http\Exception\NotAcceptable;
use WeeChat\Core\Http\Exception\NotAllowed;
use WeeChat\Core\Http\Exception\NotFound;
use WeeChat\Core\Ui\Controllers\BaseController;

class MessageController extends BaseController {

  /** @var \WeeChat\Core\Repositories\MessageRepository */
  private $messageRepository;

  protected function preExecute() {
    if (!$this->currentUser->isAuthenticated()) {
      $this->redirectToReferrer();
    }


    $this->messageRepository = $this->container->getService('messages');
  }

  //  /message/delete/15

  public function preRenderDeleteMessage($message_id) {
    /** @var \WeeChat\Core\Chat\Message\TextMessage $message */
    $message = $this->messageRepository->get($message_id);

    if ($message === FALSE) {
      throw new NotAcceptable();
    }

    if ($message->getSender()->getId() !== $this->currentUser->getId()) {
      throw new NotAllowed();
    }

    $this->messageRepository->delete($message);

    $this->redirectToReferrer();
  }

}
