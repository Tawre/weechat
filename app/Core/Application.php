<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core;

use WeeChat\Core\Http\Exception\HttpException;
use WeeChat\Core\Http\Request;
use WeeChat\Core\Repositories\MessageRepository;
use WeeChat\Core\Repositories\NotificationRepository;
use WeeChat\Core\Repositories\RoomRepository;
use WeeChat\Core\Services\RouteMatcher;
use WeeChat\Core\Ui\Templating\TemplateManager;
use WeeChat\Core\Services\AssetsService;
use WeeChat\Core\Services\PathDiscover;
use WeeChat\Core\Repositories\UserRepository;

class Application {

  /** @var \WeeChat\Core\Application */
  private static $instance = NULL;

  /** @var \WeeChat\Core\Container */
  private $container;

  private $activeController;


  private $paths = [];

  /**
   * Application constructor.
   *
   * @param $request
   */
  protected function __construct() {
    $this->container = new Container($this);
    $this->registerServices();
    $this->init();
  }

  public function registerServices() {
    $this->container->addServices([
      'request' => Request::class,
      'path_discovery' => PathDiscover::class,
      'router' => RouteMatcher::class,
      'template_assets' => AssetsService::class,
      'template_manager' => TemplateManager::class,
      'users' => UserRepository::class,
      'notifications' => NotificationRepository::class,
      'rooms' => RoomRepository::class,
      'messages' => MessageRepository::class,
    ]);

    include realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' .
      DIRECTORY_SEPARATOR . '..' .
      DIRECTORY_SEPARATOR . 'config' .
      DIRECTORY_SEPARATOR . 'database.php');

    $this->container->addService('database', 'PDO', [
      "mysql:dbname={$database_name};host={$database_host};port={$database_port}",
      $database_user,
      $database_password,
      [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ],
    ]);

  }

  protected function render() {
    /** @var RouteMatcher $routeMatcher */
    $routeMatcher = $this->container->getService('router');

    try {
      $this->activeController = $routeMatcher->findController();
      $this->getResponse();
    } catch (HttpException $e) {
      $this->activeController = $routeMatcher->getError($e->getCode());
    } catch (\Exception $e) {
      error_log($e->getMessage());
      $this->activeController = $routeMatcher->getError(500);
    } finally {
      $this->getResponse()->send();
    }
  }

  private function init() {
    session_start();

    /** @var UserRepository $userRepository */
    $userRepository = $this->container->getService('users');
    $currentUser = $userRepository->getCurrentUser();

    if ($currentUser->isAuthenticated()) {
      $userRepository->updateOnline($currentUser);
      session_regenerate_id();
    }
  }

  private function getResponse() {
    /** @var \ReflectionClass $reflection */
    $reflection = new \ReflectionClass($this->activeController['class']);

    /** @var \WeeChat\Core\Ui\Controllers\BaseController $controller */
    $controller = $reflection->newInstanceArgs([
      $this->container,
      $this->activeController['action'],
    ]);

    return $controller
      ->execute($this->activeController['arguments']);
  }

  /**
   * @return \WeeChat\Core\Application
   */
  public static function getInstance(): self {
    if (static::$instance === NULL) {
      static::$instance = new static();
    }

    return static::$instance;
  }

  public static function run() {
    $instance = static::getInstance();
    $instance->render();
  }

  /**
   * @return \WeeChat\Core\Container
   */
  public function getContainer(): \WeeChat\Core\Container {
    return $this->container;
  }


}
