<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Chat\Message;

use WeeChat\Core\User\RecepientInterface;
use WeeChat\Core\User\SenderInterface;

interface MessageInterface extends SenderInterface {

  public function getMessage();

  public function getContentString(): string;

  public function setMessage($content);

}
