<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Chat\Message;


use WeeChat\Core\Application;
use WeeChat\Core\User\User;
use WeeChat\Core\User\UserInterface;
use WeeChat\Core\Utils\JsonSerialize;

class TextMessage implements MessageInterface {

  use JsonSerialize {
    toArray as protected toArrayTrait;
  }

  private $message = "";

  private $id;

  private $sender_id;

  private $sender;

  /** @var \WeeChat\Core\Repositories\UserRepository */
  private $userRepository;

  /**
   * TextMessage constructor.
   */
  public function __construct() {
    $this->serializeableFields = [
      'id',
      'message',
    ];

    $this->userRepository = Application::getInstance()->getContainer()->getService('users');
  }

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  public function getMessage() {
    return $this->message;
  }

  public function getContentString(): string {
    return (string) $this->message;
  }

  public function setMessage($message) {
    $this->message = $message;
  }

  public function getSenderName(): string {
    return $this->getSender()->getUsername();
  }

  public function toArray() {
    $data = $this->toArrayTrait();

    $data['sender'] = $this->getSenderName();
    $data['can-delete'] = $this->canDelete();

    return $data;
  }

  public function getSender(): UserInterface {
    if ($this->sender_id) {
      return $this->userRepository->findById((int) $this->sender_id);
    }

    return new User();
  }

  public function canDelete() {
    return $this->userRepository->getCurrentUser()->getId() === $this->getSender()->getId();
  }
}
