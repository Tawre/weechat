<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Chat\Notification;


use WeeChat\Core\Chat\Notification\Event\EventInterface;
use WeeChat\Core\User\UserInterface;

interface NotificationInterface {

  public function getEvent(): EventInterface;

  public function getUser(): UserInterface;

  public function getId(): int;

  public function isReaded(): bool;

  public function toArray(): array;

}
