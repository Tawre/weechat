<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Chat\Notification\Event;


class EventNewMessage implements EventInterface {

  /** @var \WeeChat\Core\User\UserInterface */
  private $sender;

  /** @var \WeeChat\Core\Chat\ChatRoom */
  private $room;

  /**
   * EventNewMessage constructor.
   *
   * @param \WeeChat\Core\User\UserInterface $sender
   * @param \WeeChat\Core\Chat\ChatRoom $room
   */
  public function __construct(\WeeChat\Core\User\UserInterface $sender, \WeeChat\Core\Chat\ChatRoom $room) {
    $this->sender = $sender;
    $this->room = $room;
  }

  /**
   * @return \WeeChat\Core\User\UserInterface
   */
  public function getSender(): \WeeChat\Core\User\UserInterface {
    return $this->sender;
  }

  /**
   * @return \WeeChat\Core\Chat\ChatRoom
   */
  public function getRoom(): \WeeChat\Core\Chat\ChatRoom {
    return $this->room;
  }

  public function getType(): string {
    return 'new_message';
  }

  public function getMessage(): string {
    if ($this->getRoom()->isPublic()) {
      return "New message from {$this->getSender()->getUsername()} in {$this->getRoom()->getName()}";
    }

    return "New private message from {$this->getSender()->getUsername()}";
  }

  public function getLink(): string {
    return "/room/{$this->getRoom()->getId()}";
  }

  public function toArray(): array {
    return [
      'message' => $this->getMessage(),
      'link' => $this->getLink(),
    ];
  }
}
