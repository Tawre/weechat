<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Chat\Notification\Event;


interface EventInterface {

  public function getType(): string;

  public function getLink(): string;

  public function getMessage(): string;

  public function toArray(): array;
}
