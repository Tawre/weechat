<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Chat\Notification;


use WeeChat\Core\Application;
use WeeChat\Core\Chat\Notification\Event\EventInterface;
use WeeChat\Core\User\UserInterface;

class Notification implements NotificationInterface {

  /** @var int */
  private $id;

  /** @var string */
  private $type;

  /** @var string */
  private $data;

  /** @var int */
  private $user_id;

  /** @var bool */
  private $readed = FALSE;

  /** @var string Datetime */
  private $created_at;

  /** @var \WeeChat\Core\User\UserInterface */
  private $user;

  /** @var \WeeChat\Core\Chat\Notification\Event\EventInterface|null */
  private $event = NULL;

  /** @var \WeeChat\Core\Repositories\UserRepository */
  private $userRepository;

  public function __construct() {
    $this->userRepository = Application::getInstance()->getContainer()->getService('users');
  }

  public function getEvent(): EventInterface {
    if ($this->event === NULL) {
      $this->event = unserialize($this->data);
    }

    return $this->event;
  }

  public function getUser(): UserInterface {
    if ($this->user_id && $this->user === NULL) {
      $this->user = $this->userRepository->get($this->user_id);
    }

    return $this->user;
  }


  public function getId(): int {
    return (int) $this->id;
  }

  public function isReaded(): bool {
    return (bool) $this->readed;
  }


  public function toArray(): array {
    return [
        'id' => $this->getId(),
        'created' => $this->created_at,
        'readed' => $this->isReaded(),
      ] + $this->getEvent()->toArray();
  }
}
