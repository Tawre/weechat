<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Chat;

use WeeChat\Core\Application;

class ChatRoom {

  /** @var \WeeChat\Core\Repositories\UserRepository */
  private $userRepository;

  /** @var \WeeChat\Core\Repositories\MessageRepository */
  private $messageRepository;


  /** @var int */
  private $id;

  /** @var string */
  private $name;

  /** @var bool */
  private $public;

  /** @var integer|null */
  private $author_id;

  /** @var \WeeChat\Core\User\User|null */
  private $author;

  /** @var \WeeChat\Core\User\UserInterface[] */
  private $participants;

  /** @var \WeeChat\Core\Chat\Message\MessageInterface[] */
  private $messages;

  /**
   * @return \WeeChat\Core\Repositories\UserRepository
   */
  protected function users() {
    static $userRepository;
    if ($userRepository === NULL) {
      $userRepository = Application::getInstance()->getContainer()->getService('users');
    }

    return $userRepository;
  }

  /**
   * @return \WeeChat\Core\Repositories\MessageRepository
   */
  protected function messages() {
    static $messageRepository;
    if ($messageRepository === NULL) {
      $messageRepository = Application::getInstance()->getContainer()->getService('messages');
    }

    return $messageRepository;
  }

  /**
   * @return int
   */
  public function getId(): int {
    return (int) $this->id;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * @return bool
   */
  public function isPublic(): bool {
    return (bool) $this->public;
  }

  /**
   * @return \WeeChat\Core\User\User|null
   */
  public function getAuthor(): \WeeChat\Core\User\User {
    if ($this->author_id && $this->author === NULL) {
      $this->author = $this->users()->get($this->author_id);
    }

    return $this->author;
  }

  /**
   * @return mixed
   */
  public function getParticipants() {
    if ($this->participants === NULL) {
      $this->participants = $this->users()->getAllInRoom($this);
    }

    return $this->participants;
  }

  /**
   * @return mixed
   */
  public function getMessages() {
    if ($this->messages === NULL) {
      $this->messages = $this->messages()->getMessages($this);
    }

    return $this->messages;
  }


}
