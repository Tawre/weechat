<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Services;


use WeeChat\Controllers\ChatController;
use WeeChat\Controllers\HomepageController;
use WeeChat\Controllers\MessageController;
use WeeChat\Controllers\NotificationController;
use WeeChat\Controllers\SignController;
use WeeChat\Core\Container;
use WeeChat\Core\Ui\Controllers\ErrorController;

class RouteMatcher {

  /** @var \WeeChat\Core\Http\Request */
  protected $request;

  /**
   * RouteMatcher constructor.
   *
   * @param \WeeChat\Core\Http\Request $request
   */
  public function __construct(\WeeChat\Core\Http\Request $request) {
    $this->request = $request;
  }

  protected function getRoutes() {
    $routes = [
      '/' => [
        'class' => HomepageController::class,
      ],
      '/sign-in' => [
        'class' => SignController::class,
        'action' => 'in',
      ],
      '/sign-on' => [
        'class' => SignController::class,
        'action' => 'on',
      ],
      '/logout' => [
        'class' => SignController::class,
        'action' => 'out',
      ],
      '/notify-all-readed' => [
        'class' => NotificationController::class,
        'action' => 'userMarkAllReaded',
      ],
      '/api/notifications' => [
        'class' => NotificationController::class,
        'action' => 'userNotifications',
      ],

      '/chat/create-room' => [
        'class' => ChatController::class,
      ],
      '/rooms' => [
        'class' => ChatController::class,
      ],
      '/create-room' => [
        'class' => ChatController::class,
        'action' => 'createPublicRoom',
      ],
      '/^\/create-private-room\/(?<user_id>[0-9]+)/' => [
        'class' => ChatController::class,
        'action' => 'createPrivateRoom',
        'regex' => TRUE,
      ],
      '/^\/room\/(?<id>[0-9]+)/' => [
        'class' => ChatController::class,
        'action' => 'room',
        'regex' => TRUE,
      ],
      '/^\/api\/room\/(?<id>[0-9]+)\/refresh/' => [
        'class' => ChatController::class,
        'action' => 'refreshRoom',
        'regex' => TRUE,
      ],
      '/^\/message\/delete\/(?<id>[0-9]+)/' => [
        'class' => MessageController::class,
        'action' => 'deleteMessage',
        'regex' => TRUE,
      ],
    ];

    return $routes;
  }

  public function findController() {
    $uri = $this->request->getUri();

    $routes = $this->getRoutes();

    foreach ($routes as $route_uri => $route) {
      $isRegex = $route['regex'] ?? FALSE;

      if ($isRegex && preg_match($route_uri, $uri, $matches)) {
        $matches = array_filter($matches, [
          $this,
          'filterRouteRegexMatches',
        ], ARRAY_FILTER_USE_KEY);

        foreach ($matches as &$match) {
          if (is_numeric($match)) {
            $match = (int) $match;
          }
        }

        return [
          'class' => $route['class'],
          'action' => $route['action'] ?? 'default',
          'arguments' => $matches,
        ];
      }
      elseif ($route_uri == $uri) {

        return [
          'class' => $route['class'],
          'action' => $route['action'] ?? 'default',
          'arguments' => $route['arguments'] ?? [],
        ];
      }
    }

    return [
      'class' => ErrorController::class,
      'action' => '404',
      'arguments' => [],
    ];
  }

  function getError($status_code) {
    return [
      'class' => ErrorController::class,
      'action' => (int) $status_code,
      'arguments' => [],
    ];
  }

  protected function filterRouteRegexMatches($match_key) {
    return !is_numeric($match_key);
  }


  public static function create(Container $container) {
    return new static($container->getService('request'));
  }

}
