<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Services;


class AssetsService {

  protected $scripts = [];

  protected $styles = [];


  public function addJS($file) {
    $this->scripts[] = $file;
  }

  public function addCSS($file) {
    $this->styles[] = $file;
  }

  public function renderScriptTags(string $basePath = '/') {
    $output = "";

    foreach (array_unique($this->scripts) as $file) {
      $output .= "<script src='{$basePath}{$file}'></script>" . PHP_EOL;
    }

    return $output;
  }

  public function renderStyleTags(string $basePath = '/') {
    $output = "";

    foreach (array_unique($this->styles) as $file) {
      $output .= "<link href='{$basePath}{$file}' rel='stylesheet' type='text/css'>" . PHP_EOL;
    }

    return $output;
  }

  /**
   * Only for composer
   */
  public static function copyBootstrapAssets() {
  }

}
