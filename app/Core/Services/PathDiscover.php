<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Services;


class PathDiscover {

  protected $paths = [];

  /**
   * PathsManager constructor.
   *
   * @param $paths
   */
  public function __construct() {
    $this->discoverPaths();
  }

  protected function discoverPaths() {
    $this->setPath('public', realpath('./'));
    $this->setPath('app', $this->findPath('app'));
    $this->setPath('root', dirname($this->getPath('app')));
    $this->setPath('templates', $this->findPath('templates'));
  }

  private function findPath(string $path) {
    $dir = realpath('./');
    while (($dir = dirname($dir)) != '.') {
      if (file_exists($dir . DIRECTORY_SEPARATOR . $path)) {
        return $dir . DIRECTORY_SEPARATOR . $path;
      }
    }

    return NULL;
  }

  public function getRootPath() {
    return $this->getPath('root');
  }

  public function getAppPath() {
    return $this->getPath('app');
  }

  public function getTemplatesPath() {
    return $this->getPath('templates');
  }

  public function getPublicPath() {
    return $this->getPath('public');
  }

  /**
   * @return mixed
   */
  public function getPaths() {
    return $this->paths;
  }

  public function getPath(string $id) {
    return $this->paths[$id] ?? NULL;
  }

  public function setPath(string $id, string $path) {
    if ($path !== NULL) {
      if (file_exists($path)) {
        $this->paths[$id] = $path;
      }
      else {
        throw new \Exception("Path '$path' doesn't exists");
      }
    }
  }

}
