<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core;


class Container {

  protected $services;

  protected $services_instance;

  public function __construct(Application $application) {
    $this->services['app'] = [
      'class' => Application::class,
    ];

    $this->services_instance['app'] = $application;
  }


  public function getService(string $id) {
    if (isset($this->services_instance[$id])) {
      return $this->services_instance[$id];
    }

    return $this->createService($id);
  }

  public function addServices($services) {
    foreach ($services as $id => $class) {
      $this->addService($id, $class);
    }
  }

  public function addService(string $id, string $class, $args = []) {
    if (!isset($this->services[$id])) {
      $this->services[$id] = [
        'class' => $class,
        'args' => $args,
      ];
      return TRUE;
    }

    throw new \Exception("Service '{$id}' was already registered'");
  }

  protected function createService(string $id) {
    if (isset($this->services[$id]) && $class = $this->services[$id]['class']) {
      $reflection = new \ReflectionClass($class);

      if ($args = $this->services[$id]['args']) {
        $this->services_instance[$id] = $reflection->newInstanceArgs($args);
      }
      elseif ($reflection->hasMethod('create')) {
        $this->services_instance[$id] = $class::create($this);
      }
      else {
        $this->services_instance[$id] = $reflection->newInstance();
      }

      return $this->services_instance[$id];
    }
    else {
      throw new \Exception("Service '{$id}' not registered");
    }
  }


}
