<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Http\Exception;


use Throwable;

class NotFound extends HttpException {

  public function __construct($message = "", $code = 404, Throwable $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }


}
