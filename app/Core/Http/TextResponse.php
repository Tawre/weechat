<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Http;

class TextResponse extends Response {

  /**
   * Response constructor.
   *
   * @param mixed $content
   * @param array $headers
   * @param int $status
   */
  public function __construct(string $content, array $headers = [], int $status = 200) {
    parent::__construct($content, $headers, $status);

    $this->headers->set('Content-type', 'text/plain; charset=utf-8');
  }


}
