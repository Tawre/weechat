<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Http;

class JsonResponse extends Response {

  /**
   * Response constructor.
   *
   * @param mixed $content
   * @param array $headers
   * @param int $status
   */
  public function __construct($content, array $headers = [], int $status = 200) {
    parent::__construct(json_encode($content), $headers, $status);

    $this->headers->set('Content-type', 'application/json; charset=utf-8');
  }

  /**
   * @param mixed $content
   */
  public function setContent($content) {
    parent::setContent(json_encode($content));
  }


}
