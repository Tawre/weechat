<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Http;

class RedirectResponse extends Response {

  /**
   * Response constructor.
   *
   * @param string $location
   */
  public function __construct(string $location) {
    parent::__construct("", [], 302);

    $this->headers->set('Location', $location);
  }

}
