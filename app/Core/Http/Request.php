<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Http;


use WeeChat\Core\Http\Exception\NotAllowed;

class Request {

  /**
   * @return string
   * @throws \WeeChat\Core\Http\Exception\HttpException
   */
  public function getUri(): string {
    if (isset($_SERVER['PATH_INFO'])) {
      return $_SERVER['PATH_INFO'];
    }
    elseif (isset($_SERVER['REQUEST_URI'])) {
      return $_SERVER['REQUEST_URI'];
    }

    throw new NotAllowed();
  }

  public function getReferrer () {
    return $_SERVER['HTTP_REFERER'] ?? '/';
  }

}
