<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Http;


use WeeChat\Core\Utils\ParameterBag;

class Response {

  protected $content = "";

  protected $headers;

  protected $status = 200;

  /**
   * Response constructor.
   *
   * @param string $content
   * @param array $headers
   * @param int $status
   */
  public function __construct(string $content = "", array $headers = [], int $status = 200) {
    $this->content = $content;
    $this->headers = new ParameterBag($headers);
    $this->status = $status;
  }

  public function send() {
    $this->sendHeaders();

    echo $this->content;
  }

  /**
   * @return string
   */
  public function getContent(): string {
    return $this->content;
  }

  /**
   * @return \WeeChat\Core\Utils\ParameterBag
   */
  public function getHeaders(): \WeeChat\Core\Utils\ParameterBag {
    return $this->headers;
  }

  /**
   * @return int
   */
  public function getStatus(): int {
    return $this->status;
  }

  /**
   * @param string $content
   */
  public function setContent($content) {
    $this->content = $content;
  }

  /**
   * @param int $status
   */
  public function setStatus(int $status) {
    $this->status = $status;
  }


  public function sendHeaders() {
    http_response_code($this->status);

    foreach ($this->headers->getRawData() as $header => $value) {
      header("{$header}: $value");
    }
  }


}
