<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Composer;

use WeeChat\Core\Utils\ClassFinder;

class ComposerTasker {

  protected static function getTasks() {
    static $tasks = [];

    if (empty($tasks)) {
      $namespace = __NAMESPACE__ . ClassFinder::NAMESPACE_SEPARATOR . "Task";
      $taskClasses = ClassFinder::getClasses($namespace);


      foreach ($taskClasses as $task) {
        $tasks[] = new $task();
      }
    }

    return $tasks;
  }

  public static function postUpdate() {
    $tasks = static::getTasks();

    /** @var \WeeChat\Core\Composer\BaseTask $task */
    foreach ($tasks as $task) {
      $task->postUpdate();
    }
  }

}
