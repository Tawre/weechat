<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Composer\Task;


use WeeChat\Core\Composer\BaseTask;

class BootstrapCopyDistributionFilesTask extends BaseTask {

  public function postUpdate() {

    $boostrapPath = realpath('./vendor/twbs/bootstrap/dist');
    $publicPath = realpath('./public');

    $files = [
      'css/bootstrap.min.css',
      'css/bootstrap-grid.min.css',
    ];

    foreach ($files as $file) {
      copy($boostrapPath . DIRECTORY_SEPARATOR . $file,
        $publicPath . DIRECTORY_SEPARATOR . $file);

      $filename = basename($file);
      echo "Boostrap distribution file '{$filename}' copied" . PHP_EOL;
    }
  }


}
