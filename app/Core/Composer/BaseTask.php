<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Composer;

abstract class BaseTask {

  public function postUpdate() {
  }

}
