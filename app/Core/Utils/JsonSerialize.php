<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Utils;


trait JsonSerialize {

  protected $serializeableFields = [];

  public function toArray() {
    $reflection = new \ReflectionClass($this);

    $output = [];

    foreach ($this->serializeableFields as $serializeableField) {
      $property = $reflection->getProperty($serializeableField);
      $property->setAccessible(TRUE);
      $output[$serializeableField] = $property->getValue($this);
    }
    return $output;
  }

  public function toJson() {
    return json_encode($this->toArray());
  }

}
