<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Utils;

class ParameterBag {

  protected $data;

  /**
   * ParameterBag constructor.
   *
   * @param array $data
   */
  public function __construct(array $data = []) {
    $this->data = $data;
  }


  public function set($key, $value) {
    $this->data[$key] = $value;
  }

  public function get($key) {
    return $this->data[$key] ?? NULL;
  }

  /**
   * @return array
   */
  public function getRawData(): array {
    return $this->data;
  }

  /**
   * @param array $data
   */
  public function setRawData(array $data) {
    $this->data = $data;
  }


}
