<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Utils;


class ClassFinder {

  const NAMESPACE_SEPARATOR = "\\";

  public static function getClasses($namespace = 'WeeChat') {
    $baseNamespace = substr($namespace, 0, 7);
    if ($baseNamespace !== 'WeeChat') {
      echo "Sorry, class discovery cannot be done on namespace not starting within 'WeeChat'";
      return [];
    }

    $basePath = realpath(__DIR__ .
      DIRECTORY_SEPARATOR . '..' .
      DIRECTORY_SEPARATOR . '..');

    $directoryPath = substr($namespace, 8);
    $relativePath = $directoryPath ?
      str_replace(self::NAMESPACE_SEPARATOR, DIRECTORY_SEPARATOR, $directoryPath) :
      "";

    $completePath = $basePath . DIRECTORY_SEPARATOR . $relativePath;

    $directoryIterator = new \DirectoryIterator($completePath);
    $phpFiles = new \RegexIterator($directoryIterator, '/\.php$/');

    $prefixedNamespace = $namespace . self::NAMESPACE_SEPARATOR;

    $foundedClasses = [];
    /** @var \SplFileInfo $phpFile */
    foreach ($phpFiles as $phpFile) {
      $class = substr($phpFile->getFilename(), 0, -4);
      $foundClass = $prefixedNamespace . $class;

      if (class_exists($foundClass)) {
        $foundedClasses[] = $foundClass;
      }
    }

    return $foundedClasses;
  }

}
