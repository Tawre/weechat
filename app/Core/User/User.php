<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\User;


use WeeChat\Core\Utils\JsonSerialize;

class User implements UserInterface {

  use JsonSerialize {
    toArray as protected traitToArray;
  }

  private $id = 0;

  private $username = 'Anonymous';

  private $email;

  private $password;

  private $last_activity;

  private $authenticated = FALSE;

  public function __construct() {
    $this->serializeableFields = [
      'id',
      'username',
      'last_activity',
    ];
  }


  public function getId(): int {
    return (int) $this->id;
  }

  public function getUsername(): string {
    return $this->username;
  }

  public function getEmail(): string {
    return $this->email;
  }

  public function isAuthenticated(): bool {
    return $this->authenticated;
  }

  public function verifyPassword($password) {
    return password_verify($password, $this->password);
  }

  public function authenticated() {
    $this->authenticated = TRUE;
  }

  /**
   * @return \DateTime
   */
  public function getLastOnline() {
    $last_online = \DateTime::createFromFormat('Y-m-d H:i:s', $this->last_activity);

    return $last_online;
  }

  public function isOnline(): bool {
    return time() - 15 < $this->getLastOnline()->getTimestamp();
  }

  public function toArray() {
    $data = $this->traitToArray();

    $data['is-online'] = $this->isOnline();

    return $data;
  }


}
