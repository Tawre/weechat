<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\User;


interface UserInterface {

  public function getId(): int;

  public function getUsername(): string;

  public function getEmail(): string;

  public function isAuthenticated(): bool;

  public function isOnline(): bool;

}
