<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\User;


interface RecepientInterface {

  public function getRecepient(): UserInterface;

}
