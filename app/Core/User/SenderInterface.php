<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\User;


interface SenderInterface {

  public function getSender(): UserInterface;

}
