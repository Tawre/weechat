<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Repositories;


use WeeChat\Core\Chat\ChatRoom;
use WeeChat\Core\Chat\Message\TextMessage;
use WeeChat\Core\User\UserInterface;

class MessageRepository extends BaseRepository {

  public function getMessages(ChatRoom $room) {
    $statement = $this->database->prepare('SELECT * FROM message WHERE room_id = :roomId ORDER BY `posted` DESC LIMIT 10');

    $statement->execute([
      ':roomId' => $room->getId(),
    ]);

    return array_reverse($statement->fetchAll(\PDO::FETCH_CLASS, TextMessage::class));
  }

  public function addMessage(string $message, UserInterface $user, ChatRoom $room) {
    $statement = $this->database->prepare("INSERT INTO message (posted, message, sender_id, room_id) " .
      "VALUES (:posted, :message, :sender_id, :room_id)");

    $statement->execute([
      ':posted' => date('Y-m-d H:i:s'),
      ':message' => $message,
      ':sender_id' => $user->getId(),
      ':room_id' => $room->getId(),
    ]);
  }

  /**
   * @param int $message_id
   *
   * @return TextMessage|FALSE
   */
  public function get(int $message_id) {
    $statement = $this->database->prepare('SELECT * FROM message WHERE id = :messageId LIMIT 1');

    $statement->execute([
      ':messageId' => $message_id,
    ]);

    return $statement->fetchObject(TextMessage::class);
  }

  public function delete(TextMessage $message) {
    $statement = $this->database->prepare("DELETE FROM message WHERE id = :messageId");

    $statement->execute([
      ':messageId' => $message->getId(),
    ]);
  }
}
