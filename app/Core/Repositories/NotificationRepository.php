<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Repositories;


use WeeChat\Core\Chat\ChatRoom;
use WeeChat\Core\Chat\Notification\Event\EventInterface;
use WeeChat\Core\Chat\Notification\Notification;
use WeeChat\Core\User\UserInterface;

class NotificationRepository extends BaseRepository {

  public function findForUser(UserInterface $user) {
    $statement = $this->database->prepare("SELECT * FROM notification WHERE user_id = :userId ORDER BY readed ASC, created_at DESC LIMIT 20");

    $statement->execute([
      ':userId' => $user->getId(),
    ]);

    return $statement->fetchAll(\PDO::FETCH_CLASS, Notification::class);
  }

  public function countUnreadedForUser(UserInterface $user)  {
    $statement = $this->database->prepare("SELECT COUNT(*) FROM notification WHERE user_id = :userId AND readed = 0");

    $statement->execute([
      ':userId' => $user->getId(),
    ]);

    return (int) $statement->fetch(\PDO::FETCH_COLUMN);
  }

  public function addToRoom(ChatRoom $room, EventInterface $event, UserInterface $sender) {
    $statment = $this->database->prepare(
      "INSERT INTO notification (`type`, `source`, `data`,`user_id`,`created_at`)
    SELECT :type, :source, :data, `user_id`, NOW() 
    FROM `room_user`
    WHERE `user_id` <> :userId AND room_id = :roomId");

    $statment->execute([
      ':type' => $event->getType(),
      ':source' => "room:{$room->getId()}",
//      ':data' => serialize(NULL),
      ':data' => serialize($event),
      ':userId' => $sender->getId(),
      ':roomId' => $room->getId(),
    ]);
  }

  public function readRoomNotifications(UserInterface $user, ChatRoom $room) {
    $statement = $this->database->prepare("UPDATE notification SET readed = 1 WHERE user_id = :userId AND source = :source AND readed = 0");

    $statement->execute([
      ':userId' => $user->getId(),
      ':source' => "room:{$room->getId()}",
    ]);
  }

  public function readAllNotifications(UserInterface $user) {
    $statement = $this->database->prepare("UPDATE notification SET readed = 1 WHERE user_id = :userId AND readed = 0");

    $statement->execute([
      ':userId' => $user->getId(),
    ]);
  }

}
