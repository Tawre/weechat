<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Repositories;


use WeeChat\Core\Container;

abstract class BaseRepository {

  /** @var \PDO */
  protected $database;

  /**
   * UserRepository constructor.
   *
   * @param \PDO $database
   */
  public function __construct(\PDO $database) {
    $this->database = $database;
  }

  public static function create(Container $container) {
    return new static(
      $container->getService('database')
    );
  }
}
