<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Repositories;


use WeeChat\Core\Chat\ChatRoom;
use WeeChat\Core\User\User;
use WeeChat\Core\User\UserInterface;

class UserRepository extends BaseRepository {

  public function find(string $username) {
    $statement = $this->database->prepare("SELECT * FROM user WHERE username LIKE :username");

    $statement->execute([
      ':username' => "%" . $username . "%",
    ]);

    return $statement->fetchAll(\PDO::FETCH_CLASS, User::class);
  }

  public function findById(int $user_id) {
    $statement = $this->database->prepare("SELECT * FROM user WHERE id = :userId");

    $statement->execute([
      ':userId' => $user_id,
    ]);

    return $statement->fetchObject(User::class);
  }

  public function getAll() {
    $statement = $this->database->prepare("SELECT * FROM user");

    $statement->execute();

    return $statement->fetchAll(\PDO::FETCH_CLASS, User::class);
  }

  public function getAllOther(UserInterface $user) {
    $statement = $this->database->prepare("SELECT * FROM user WHERE id <> :userId");

    $statement->execute([
      ':userId' => $user->getId(),
    ]);

    return $statement->fetchAll(\PDO::FETCH_CLASS, User::class);
  }

  public function getAllInRoom(ChatRoom $room) {
    $statement = $this->database->prepare("
SELECT u.* FROM `room_user` ru 
INNER JOIN `user` u ON u.id = ru.user_id AND ru.room_id = :roomId
ORDER BY u.last_activity DESC");

    $statement->execute([
      ':roomId' => $room->getId(),
    ]);

    return $statement->fetchAll(\PDO::FETCH_CLASS, User::class);
  }

  public function register(string $username, string $password, string $email) {

    $hashedPassword = password_hash($password, CRYPT_BLOWFISH);

    $statement = $this->database->prepare(
      "INSERT INTO user (`username`, `email`, `password`) VALUES (:username, :email, :password)"
    );

    $statement->execute([
      ':username' => $username,
      ':email' => $email,
      ':password' => $hashedPassword,
    ]);
  }

  public function login(string $username, string $password) {

    $statement = $this->database->prepare(
      "SELECT * FROM user WHERE username = :username"
    );

    $statement->execute([
      ':username' => $username,
    ]);

    /** @var User $user */
    $user = $statement->fetchObject(User::class);

    if ($user && $user->verifyPassword($password)) {
      $user->authenticated();
      $_SESSION['current_user'] = $user;
      return $user;
    }

    return NULL;
  }

  public function logout() {
    $_SESSION['current_user'] = NULL;
  }

  public function isUserLoggedIn() {
    return isset($_SESSION['current_user']) && $_SESSION['current_user']->isAuthenticated();
  }

  public function getCurrentUser() {
    return $_SESSION['current_user'] ?? new User();
  }

  public function updateOnline(UserInterface $user) {
    $statement = $this->database->prepare('UPDATE `user` SET last_activity = :last_activity WHERE id = :userId');

    $statement->execute([
      ':last_activity' => date('Y-m-d H:i:s'),
      ':userId' => $user->getId(),
    ]);
  }


  public function get(int $userId) {

    $statement = $this->database->prepare(
      "SELECT * FROM user WHERE id = :userId"
    );

    $statement->execute([
      ':userId' => $userId,
    ]);

    /** @var User $user */
    return $statement->fetchObject(User::class);
  }

}
