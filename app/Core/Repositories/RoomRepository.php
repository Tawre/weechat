<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Repositories;


use WeeChat\Core\Chat\ChatRoom;
use WeeChat\Core\Chat\Message\TextMessage;
use WeeChat\Core\User\UserInterface;

class RoomRepository extends BaseRepository {

  public function getAllPublic() {
    $statement = $this->database->prepare("SELECT * FROM room WHERE public = :public");

    $statement->execute([
      ':public' => 1,
    ]);

    return $statement->fetchAll(\PDO::FETCH_CLASS, ChatRoom::class);
  }

  public function getRoom(int $id) {
    $statement = $this->database->prepare("SELECT * FROM room WHERE id = :id");

    $statement->execute([
      ':id' => $id,
    ]);

    return $statement->fetchObject(ChatRoom::class);
  }

  public function addParticipant(ChatRoom $room, UserInterface $user) {
    $statement = $this->database->prepare(
      "INSERT INTO room_user (`user_id`, `room_id`) 
VALUES (:userId, :roomId) ON DUPLICATE KEY UPDATE `user_id` = :userId, `room_id` = :roomId");

    $statement->execute([
      ':userId' => $user->getId(),
      ':roomId' => $room->getId(),
    ]);
  }

  public function createRoom($roomName, $public, UserInterface $author = NULL) {
    $statement = $this->database->prepare("INSERT INTO room (`name`, `public`, `author_id`) VALUES (:roomName, :public, :authorId)");

    $statement->execute([
      ':roomName' => $roomName,
      ':public' => (int) $public,
      ':authorId' => $author && $author->isAuthenticated() ? $author->getId() : NULL,
    ]);

    return $this->getRoom($this->database->lastInsertId());
  }

  public function findPrivateRoom(UserInterface $sender, UserInterface $receiver) {
    $statement = $this->database->prepare(
      "SELECT r.* FROM room r " .
      "INNER JOIN (" .
      "     SELECT room_id, COUNT(user_id) AS count_participants FROM room_user GROUP BY room_id " .
      ") ruc ON ruc.room_id = r.id AND ruc.count_participants = 2 " .
      "INNER JOIN (" .
      "     SELECT room_id FROM room_user " .
      "     WHERE user_id IN (:senderId, :receiverId) " .
      "     GROUP BY room_id " .
      "     HAVING COUNT(user_id) = 2 " .
      ") ru ON ruc.room_id = ru.room_id " .
      "WHERE r.public = 0 ".
      "GROUP BY r.id"
    );

    $statement->execute([
      ':senderId' => $sender->getId(),
      ':receiverId' => $receiver->getId(),
    ]);

    return $statement->fetchObject(ChatRoom::class);
  }

  public function getPrivateRoom(UserInterface $sender, UserInterface $receiver) {
    $room = $this->findPrivateRoom($sender, $receiver);

    if ($room === FALSE) {
      $room = $this->createRoom('Private chat', FALSE, $sender);

      $this->addParticipant($room, $sender);
      $this->addParticipant($room, $receiver);
    }

    return $room;
  }

}
