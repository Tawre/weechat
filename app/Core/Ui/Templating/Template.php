<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Ui\Templating;

use WeeChat\Core\Services\AssetsService;

class Template {

  /** @var string */
  private $file;

  /** @var array */
  private $data = [];

  private $templatePath;

  private $basePath;

  /** @var AssetsService */
  protected $assetsManager;

  /**
   * Template constructor.
   *
   * @param string $file
   * @param string $templatePath
   * @param string $basePath
   * @param AssetsService $assetsManager
   */
  public function __construct(string $file, string $templatePath, string $basePath, AssetsService $assetsManager) {
    $this->file = $file;
    $this->templatePath = $templatePath;
    $this->assetsManager = $assetsManager;
    $this->data = [];
    $this->basePath = $basePath;
  }

  /**
   * @return string
   */
  public function getFile(): string {
    return $this->file;
  }

  /**
   * @param string $file
   */
  public function setFile(string $file) {
    $this->file = $file;
  }


  public function __get($name) {
    return $this->data[$name] ?? NULL;
  }

  public function __set($name, $value) {
    $this->data[$name] = $value;
  }

  public function __isset($name) {
    return isset($this->data[$name]);
  }

  public function render(bool $return = FALSE) {
    extract($this->data);


    $filePath = $this->templatePath . DIRECTORY_SEPARATOR . $this->file;

    if (file_exists($filePath)) {
      if ($return) {
        ob_start();
      }

      $styles = $this->assetsManager->renderStyleTags($this->basePath);
      $scripts = $this->assetsManager->renderScriptTags($this->basePath);

      include $filePath;

      if ($return) {
        return ob_get_clean();
      }
    }
    else {
      throw new \Exception(sprintf('Template "%s" not found', $this->file));
    }
  }

}
