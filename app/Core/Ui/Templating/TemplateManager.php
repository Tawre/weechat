<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Ui\Templating;


use WeeChat\Core\Container;
use WeeChat\Core\Services\AssetsService;

class TemplateManager {

  protected $templatePath;

  protected $assetsManager;

  /**
   * TemplateManager constructor.
   *
   * @param $templatePath string
   * @param $assetsManager AssetsService
   */
  public function __construct(string $templatePath, AssetsService $assetsManager, $basePath = '/') {
    $this->templatePath = $templatePath;
    $this->assetsManager = $assetsManager;
    $this->basePath = $basePath;
  }


  public static function create(Container $container) {
    /** @var \WeeChat\Core\Services\PathDiscover $pathManager */
    $pathManager = $container->getService('path_discovery');

    /** @var AssetsService $assetsManager */
    $assetsManager = $container->getService('template_assets');

    $assetsManager->addCSS('css/bootstrap.min.css');
    $assetsManager->addCSS('css/bootstrap-grid.min.css');
    $assetsManager->addCSS('css/style.css');

    return new static($pathManager->getTemplatesPath(), $container->getService('template_assets'));
  }

  public function createTemplate($file) {
    return new Template($file, $this->templatePath, $this->basePath, $this->assetsManager);
  }

}
