<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Ui\Controllers;

class ErrorController extends BaseController {

  public function execute($params = []) {
    $response = parent::execute($params);
    $response->setStatus((int) $this->action);
    return $response;
  }


}
