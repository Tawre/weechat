<?php
/**
 * @author Josef Vyhnánek <josef@vyhnanek.eu>
 */
declare(strict_types=1);

namespace WeeChat\Core\Ui\Controllers;


use WeeChat\Core\Container;
use WeeChat\Core\Http\RedirectResponse;
use WeeChat\Core\Http\Response;
use WeeChat\Core\Http\JsonResponse;
use WeeChat\Core\Http\TextResponse;
use WeeChat\Core\Services\AssetsService;

abstract class BaseController {

  /** @var \WeeChat\Core\Container */
  protected $container;

  /** @var string */
  protected $action;

  /** @var \WeeChat\Core\Ui\Templating\Template */
  protected $template;

  /** @var \WeeChat\Core\Http\Request */
  protected $request;

  /** @var \WeeChat\Core\User\User|null */
  protected $currentUser;

  /** @var array */
  private $params = [];

  /**
   * BaseController constructor.
   *
   * @param \WeeChat\Core\Container $container
   * @param string $action
   */
  public function __construct(Container $container, string $action = 'default') {
    $this->container = $container;
    $this->action = $action;
    $this->request = $container->getService('request');
    //    $this->params = $container->getService('route_matcher')->getParams();
    //    $this->response = $container->getService('response');


    $templateRelativePath = $this->templateNamespace() . DIRECTORY_SEPARATOR . $action . '.phtml';
    $this->template = $this->container
      ->getService('template_manager')
      ->createTemplate($templateRelativePath);

    /** @var \WeeChat\Core\Repositories\UserRepository $userRepositary */
    $userRepositary = $this->container->getService('users');
    $this->currentUser = $userRepositary->getCurrentUser();
    $this->template->currentUser = $this->currentUser;

    $this->template->notificationCount = 0;


    /** @var AssetsService $assetsManager */
    $assetsManager = $this->container->getService('template_assets');

    $assetsManager->addJS('js/notifications.js');
  }

  protected function preExecute() {

  }

  protected function postExecute() {

  }

  public function execute($params = []) {
    $this->preExecute();

    $methodName = 'preRender' . ucfirst($this->action);
    if (method_exists($this, $methodName)) {
      call_user_func_array([$this, $methodName], $params);
    }

    $methodName = 'render' . ucfirst($this->action);
    if (method_exists($this, $methodName)) {
      $result = call_user_func_array([$this, $methodName], $params);
    }

    $this->postExecute();

    if (isset($result)) {
      if ($result instanceof Response) {
        return $result;
      }
      elseif (is_string($result)) {
        return new TextResponse($result);
      }
      elseif (is_array($result)) {
        return new JsonResponse($result);
      }
    }

    return new Response($this->renderTemplate());
  }

  protected function templateNamespace() {
    $shortClassName = (new \ReflectionClass($this))->getShortName();

    return substr($shortClassName, 0, -10);
  }

  public function setTemplateFile($file) {
    $templateRelativeFilepath = $this->templateNamespace() . DIRECTORY_SEPARATOR . $file;
    $this->template->setFile($templateRelativeFilepath);
  }

  public function renderTemplate(): string {
    $content = $this->template->render(TRUE);

    $this->template->setFile('@layout.phtml');
    $this->template->content = $content;

    return $this->template->render(TRUE);
  }

  protected function redirectTo(string $location) {
    $response = new RedirectResponse($location);
    $response->send();
    exit();
  }

  protected function redirectToReferrer() {
    $request = $this->container->getService('request');
    $this->redirectTo($request->getReferrer());
  }

}
